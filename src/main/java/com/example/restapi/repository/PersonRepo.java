package com.example.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.restapi.model.TbPerson;

@Repository
public interface PersonRepo extends JpaRepository<TbPerson, String>{
	
	public TbPerson findByPid(String pid);
	
}
