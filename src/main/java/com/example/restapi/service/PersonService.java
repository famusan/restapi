package com.example.restapi.service;

import java.util.List;

import com.example.restapi.model.TbPerson;

public interface PersonService {

	public List<TbPerson> getAllPerson();
	
	public TbPerson getByPid(String pid);
}
