package com.example.restapi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.restapi.model.TbPerson;
import com.example.restapi.repository.PersonRepo;
import com.example.restapi.service.PersonService;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

@Autowired
private PersonRepo pr;


	@Override
	public List<TbPerson> getAllPerson() {
		List<TbPerson> tps = new ArrayList<TbPerson>();
		tps = pr.findAll();
		return tps;
	}


	@Override
	public TbPerson getByPid(String pid) {
		TbPerson tp = new TbPerson();
		tp = pr.findByPid(pid);
		return tp;
	}

}
