package com.example.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.restapi.model.TbPerson;
import com.example.restapi.service.PersonService;

@RestController
public class MainController {

	@Autowired
	private PersonService ps;

	@GetMapping("/getallpersonal")
	public List<TbPerson> getAllPerson() {
		List<TbPerson> tps = new ArrayList<TbPerson>();
		try {
			tps = ps.getAllPerson();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return tps;
	}

	@GetMapping("/getpersonalid")
	public TbPerson getIdPerson(@RequestParam String pid) {
		TbPerson tp = new TbPerson();
		try {
			tp = ps.getByPid(pid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return tp;
	}

}
