/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.restapi.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "tb_gender")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbGender.findAll", query = "SELECT t FROM TbGender t")
    , @NamedQuery(name = "TbGender.findByGender", query = "SELECT t FROM TbGender t WHERE t.gender = :gender")})
@JsonIgnoreProperties({"tbPersonCollection"})
public class TbGender implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "GENDER")
    private String gender;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gender")
    @JsonManagedReference
    private Collection<TbPerson> tbPersonCollection;

    public TbGender() {
    }

    public TbGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @XmlTransient
    public Collection<TbPerson> getTbPersonCollection() {
        return tbPersonCollection;
    }

    public void setTbPersonCollection(Collection<TbPerson> tbPersonCollection) {
        this.tbPersonCollection = tbPersonCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gender != null ? gender.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbGender)) {
            return false;
        }
        TbGender other = (TbGender) object;
        if ((this.gender == null && other.gender != null) || (this.gender != null && !this.gender.equals(other.gender))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.demo.entity.TbGender[ gender=" + gender + " ]";
    }
    
}
