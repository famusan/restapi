/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.restapi.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "tb_person")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "TbPerson.findAll", query = "SELECT t FROM TbPerson t"),
		@NamedQuery(name = "TbPerson.findByPid", query = "SELECT t FROM TbPerson t WHERE t.pid = :pid"),
		@NamedQuery(name = "TbPerson.findByFname", query = "SELECT t FROM TbPerson t WHERE t.fname = :fname"),
		@NamedQuery(name = "TbPerson.findByLname", query = "SELECT t FROM TbPerson t WHERE t.lname = :lname"),
		@NamedQuery(name = "TbPerson.findByDob", query = "SELECT t FROM TbPerson t WHERE t.dob = :dob") })
public class TbPerson implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 20)
	@Column(name = "PID")
	private String pid;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "FNAME")
	private String fname;
	@Size(max = 255)
	@Column(name = "LNAME")
	private String lname;
	@Lob
	@Size(max = 65535)
	@Column(name = "ADDRESS")
	private String address;
	@Column(name = "DOB")
	@Temporal(TemporalType.DATE)
	private Date dob;
	
	@JsonManagedReference
	@JoinTable(name = "tb_listhobby", joinColumns = {
			@JoinColumn(name = "PID", referencedColumnName = "PID") }, inverseJoinColumns = {
					@JoinColumn(name = "HID", referencedColumnName = "HID") })
	@ManyToMany(fetch = FetchType.LAZY ,cascade = {CascadeType.ALL})
	private Collection<TbHobby> tbHobbyCollection;
	
	@OneToMany(mappedBy = "idPerson")
	@JsonManagedReference
	private Collection<TbVehicle> tbVehicleCollection;
	
	@JoinColumn(name = "GENDER", referencedColumnName = "GENDER")
	@ManyToOne(optional = false)
	@JsonBackReference
	private TbGender gender;

	
	
	public TbPerson() {
	}

	public TbPerson(String pid) {
		this.pid = pid;
	}

	public TbPerson(String pid, String fname) {
		this.pid = pid;
		this.fname = fname;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@XmlTransient
	public Collection<TbHobby> getTbHobbyCollection() {
		return tbHobbyCollection;
	}

	public void setTbHobbyCollection(Collection<TbHobby> tbHobbyCollection) {
		this.tbHobbyCollection = tbHobbyCollection;
	}

	@XmlTransient
	public Collection<TbVehicle> getTbVehicleCollection() {
		return tbVehicleCollection;
	}

	public void setTbVehicleCollection(Collection<TbVehicle> tbVehicleCollection) {
		this.tbVehicleCollection = tbVehicleCollection;
	}

	public TbGender getGender() {
		return gender;
	}

	public void setGender(TbGender gender) {
		this.gender = gender;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (pid != null ? pid.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof TbPerson)) {
			return false;
		}
		TbPerson other = (TbPerson) object;
		if ((this.pid == null && other.pid != null) || (this.pid != null && !this.pid.equals(other.pid))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.example.demo.entity.TbPerson[ pid=" + pid + " ]";
	}

}
