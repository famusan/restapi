/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.restapi.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "tb_hobby")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbHobby.findAll", query = "SELECT t FROM TbHobby t")
    , @NamedQuery(name = "TbHobby.findByHid", query = "SELECT t FROM TbHobby t WHERE t.hid = :hid")
    , @NamedQuery(name = "TbHobby.findByDescription", query = "SELECT t FROM TbHobby t WHERE t.description = :description")})
public class TbHobby implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "HID")
    private Integer hid;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    
    @JsonBackReference
    @ManyToMany(mappedBy = "tbHobbyCollection")
    private Collection<TbPerson> tbPersonCollection;

    public TbHobby() {
    }

    public TbHobby(Integer hid) {
        this.hid = hid;
    }

    public Integer getHid() {
        return hid;
    }

    public void setHid(Integer hid) {
        this.hid = hid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<TbPerson> getTbPersonCollection() {
        return tbPersonCollection;
    }

    public void setTbPersonCollection(Collection<TbPerson> tbPersonCollection) {
        this.tbPersonCollection = tbPersonCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hid != null ? hid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbHobby)) {
            return false;
        }
        TbHobby other = (TbHobby) object;
        if ((this.hid == null && other.hid != null) || (this.hid != null && !this.hid.equals(other.hid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.demo.entity.TbHobby[ hid=" + hid + " ]";
    }
    
}
