/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.restapi.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "tb_vehicle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbVehicle.findAll", query = "SELECT t FROM TbVehicle t")
    , @NamedQuery(name = "TbVehicle.findByPlateNum", query = "SELECT t FROM TbVehicle t WHERE t.plateNum = :plateNum")
    , @NamedQuery(name = "TbVehicle.findByCarName", query = "SELECT t FROM TbVehicle t WHERE t.carName = :carName")})
public class TbVehicle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "PLATE_NUM")
    private String plateNum;
    @Size(max = 100)
    @Column(name = "CAR_NAME")
    private String carName;
    
    @JoinColumn(name = "ID_PERSON", referencedColumnName = "PID")
    @ManyToOne
    @JsonBackReference
    private TbPerson idPerson;

    public TbVehicle() {
    }

    public TbVehicle(String plateNum) {
        this.plateNum = plateNum;
    }

    public String getPlateNum() {
        return plateNum;
    }

    public void setPlateNum(String plateNum) {
        this.plateNum = plateNum;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public TbPerson getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(TbPerson idPerson) {
        this.idPerson = idPerson;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (plateNum != null ? plateNum.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbVehicle)) {
            return false;
        }
        TbVehicle other = (TbVehicle) object;
        if ((this.plateNum == null && other.plateNum != null) || (this.plateNum != null && !this.plateNum.equals(other.plateNum))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.demo.entity.TbVehicle[ plateNum=" + plateNum + " ]";
    }
    
}
